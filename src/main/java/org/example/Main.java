package org.example;

import java.util.*;

public class Main {

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) {
        while (true) {
            try {
                Scanner scan = new Scanner(System.in);
                System.out.print("Masukkan jumlah anak tangga: ");
                int height = scan.nextInt();

                if (height < 0 || height > 100) {
                    System.out.println("Angka tidak valid");
                } else {
                    int c = height - 1;
                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < height; j++) {
                            if (j < c) {
                                System.out.print("  ");
                            } else {
                                System.out.print(" #");
                            }
                        }
                        System.out.println();
                        c = c - 1;
                    }
                }
            } catch (Exception e) {
                System.out.println("Angka tidak valid");
            }
        }
    }
}